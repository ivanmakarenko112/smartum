import React, { Component } from 'react';
import Navigate from '@/component/navigate';
import Main from '@/component/main';

class Home extends Component {
  render() {
    return (
      <div className="core-wrapper">
        <Navigate />
        <Main />
      </div>
    );
  }
}

export default Home;
