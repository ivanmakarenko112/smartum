import analytics from '@/assets/navigation/analytics.png';
import businessBriefcaseCash from '@/assets/navigation/business-briefcase-cash.png';
import contentBook from '@/assets/navigation/content-book-2.png';
import dataUpload from '@/assets/navigation/data-upload-5.png';
import filmstrip from '@/assets/navigation/filmstrip.png';
import layer from '@/assets/navigation/layer-2.png';
import natureFlower from '@/assets/navigation/nature-flower-1.png';
import photoLandscape from '@/assets/navigation/photo-landscape.png';

export const navigationText = [
    'Open Circles',
    'Daily ',
    'Brainstorm',
    'Course',
    'Leveling system',
    'Live streams',
    'Advanced',
    'Video analytics'
];

export const navigationImg = [
    photoLandscape,
    natureFlower,
    layer,
    contentBook,
    dataUpload,
    filmstrip,
    businessBriefcaseCash,
    analytics,
];
