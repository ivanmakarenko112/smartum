export const users = [
  {
    email: 'aohlsen0@tumblr.com',
    name: {
      firstName: 'Arline',
      secondName: ' Ohlsen'
    },
    type: 'Business',
    company: 'Cogibox',
    country: 'Peru',
    subscription: '08/26/2017'
  },
  {
    email: 'tacory1@engadget.com',
    name: {
      firstName: 'Tomkin',
      secondName: ' Acory'
    },
    type: 'Business',
    company: 'Mymm',
    country: 'Uruguay',
    subscription: '03/14/2018'
  },
  {
    email: 'rgillicuddy2@amazon.com',
    name: {
      firstName: 'Rouvin',
      secondName: ' Gillicuddy'
    },
    type: 'Private',
    company: 'Eamia',
    country: 'China',
    subscription: '09/23/2016'
  },
  {
    email: 'rivakhnov3@goo.gl',
    name: {
      firstName: 'Robinson',
      secondName: ' Ivakhnov'
    },
    type: 'Private',
    company: 'Geba',
    country: 'Poland',
    subscription: '12/11/2017'
  },
  {
    email: 'tminillo4@hatena.ne.jp',
    name: {
      firstName: 'Thor',
      secondName: ' Minillo'
    },
    type: 'Business',
    company: 'Bubbletube',
    country: 'Poland',
    subscription: '06/08/2018'
  },
  {
    email: 'rkeady5@cdbaby.com',
    name: {
      firstName: 'Ramsay',
      secondName: ' Keady'
    },
    type: 'Private',
    company: 'Zooxo',
    country: 'Japan',
    subscription: '02/20/2017'
  },
  {
    email: 'cbeazley6@weather.com',
    name: {
      firstName: 'Cheryl',
      secondName: ' Beazley'
    },
    type: 'Private',
    company: 'Thoughtbeat',
    country: 'Finland',
    subscription: '10/30/2017'
  },
  {
    email: 'lcanet7@hubpages.com',
    name: {
      firstName: 'Lynelle',
      secondName: ' Canet'
    },
    type: 'Business',
    company: 'Abata',
    country: 'South Korea',
    subscription: '11/13/2017'
  },
  {
    email: 'ariglesford8@seesaa.net',
    name: {
      firstName: 'Agosto',
      secondName: ' Riglesford'
    },
    type: 'Private',
    company: 'Oloo',
    country: 'Greece',
    subscription: '12/20/2016'
  },
  {
    email: 'iwarrener9@ifeng.com',
    name: {
      firstName: 'Ivan',
      secondName: ' Warrener'
    },
    type: 'Business',
    company: 'Ntag',
    country: 'Bosnia and Herzegovina',
    subscription: '01/06/2018'
  }
];

export const columnName = ['Email', 'Name', 'Type', 'Company', 'Country', 'Subscription'];
