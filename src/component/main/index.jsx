import React, { Component } from 'react';

import Header from '@/component/header';
import UsersTable from '@/component/usersTable';
import EditUser from '@/component/editUser';
import ModalDialog from '@/blocks/modalDialog';

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpenModal: false,
      data: {}
    };
  }

  openDialog = () => {
    const { isOpenModal, data } = this.state;
    if (isOpenModal) {
      return (
        <ModalDialog close={this.handleCloseDialog}>
          <EditUser data={data} close={this.handleCloseDialog} />
        </ModalDialog>
      );
    }
    return;
  };

  handleCloseDialog = () => {
    this.setState({
      isOpenModal: false,
      data: {}
    });
  };

  handleClick = item => {
    if (item) {
      this.setState({
        isOpenModal: true,
        data: item
      });
    }
  };

  render() {
    return (
      <div className="main-wrapper">
        {this.openDialog()}
        <Header />
        <UsersTable handleClick={this.handleClick} />
      </div>
    );
  }
}

export default Main;
