import React, { Component } from 'react';

import closeEdit from '@/assets/modal/close-edit.png';
import dropDawnEditImg from '@/assets/modal/drop-dawn-edit.png';

class EditUser extends Component {
  handleClick = event => {
    event.stopPropagation();
  };

  render() {
    const { close, data } = this.props;
    return (
      <div onClick={this.handleClick} className="edit-wrapper">
        <div className="edit-header">
          <div className="edit-header-info">
            <span className="edit-header-info-first">EDIT {data.name.firstName}</span>

            <span className="edit-header-info-second">unique ID - 435325325</span>
          </div>

          <button className="edit-close" onClick={close}>
            <img src={closeEdit} alt="" />
          </button>
        </div>

        <div className="edit-body-wrapper">
          <div className="edit-body">
            <div className="edit-body-block edit-body-block-first">
              <div className="edit-field-wrapper">
                <span className="edit-field-info">E-mail</span>
                <input
                  defaultValue={data.email}
                  placeholder="E-mail"
                  type="text"
                  className="edit-field"
                />
              </div>

              <div className="edit-field-wrapper">
                <span className="edit-field-info">Country</span>
                <input
                  defaultValue={data.country}
                  placeholder="Country"
                  type="text"
                  className="edit-field"
                />
              </div>
            </div>

            <div className="edit-body-block edit-body-block-second">
              <div className="edit-body-block-name">
                <div className="edit-field-wrapper">
                  <span className="edit-field-info">First name</span>
                  <div>
                    <input
                      defaultValue={data.name.firstName}
                      placeholder="First name"
                      type="text"
                      className="edit-field"
                    />
                  </div>
                </div>

                <div className="edit-field-wrapper">
                  <span className="edit-field-info">Last Name</span>
                  <div>
                    <input
                      defaultValue={data.name.secondName}
                      placeholder="Last Name"
                      type="text"
                      className="edit-field"
                    />
                  </div>
                </div>
              </div>
              <div className="edit-field-wrapper">
                <span className="edit-field-info">Company</span>
                <input
                  defaultValue={data.company}
                  placeholder="Company"
                  type="text"
                  className="edit-field"
                />
              </div>
            </div>
          </div>

          <div className="edit-body-second">
            <div className="edit-field-wrapper">
              <span className="edit-field-info">Type</span>
              <div className="edit-drop-dawn-wrapper">
                <div className="edit-drop-dawn">
                  <span>{data.type}</span>
                  <span>
                    <img src={dropDawnEditImg} alt="" />
                  </span>
                </div>
              </div>
            </div>

            <div className="edit-field-wrapper">
              <span className="edit-field-info">level</span>
              <div className="edit-drop-dawn-wrapper">
                <div className="edit-drop-dawn">
                  <span>12</span>
                  <span>
                    <img src={dropDawnEditImg} alt="" />
                  </span>
                </div>
              </div>
            </div>

            <div className="edit-field-wrapper">
              <span className="edit-field-info">validated</span>
              <input value="Yes" type="text" className="edit-field" />
            </div>

            <div className="edit-field-wrapper">
              <span className="edit-field-info">Company</span>
              <input value="3000" placeholder="Company" type="text" className="edit-field" />
            </div>
          </div>
        </div>

        <div className="edit-footer">
          <div className="edit-footer-info-wrapper">
            <div className="edit-footer-info">
              <span className="edit-footer-info-name">Subscription type -</span>
              <span className="edit-footer-info-value">99.99</span>
            </div>

            <div className="edit-footer-info">
              <span className="edit-footer-info-name">Subscription date -</span>
              <span className="edit-footer-info-value">{data.subscription}</span>
            </div>
          </div>

          <div className="edit-footer-button-rgoup">
            <button className="edit-footer-button">Reset password</button>
            <button className="edit-footer-button">revoke access</button>
            <button className="edit-footer-button" onClick={close}>SAve</button>
          </div>
        </div>
      </div>
    );
  }
}

export default EditUser;
