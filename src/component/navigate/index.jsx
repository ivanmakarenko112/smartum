import React, { Component } from 'react';
import { navigationText, navigationImg } from '@/mock/navigation';
import logoPng from '@/assets/logo.png';

class Navigate extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 0
    };
  }

  renderNavigationList = () => {
    const { active } = this.state;
    return navigationText.map((item, index) => {
      return (
        <li key={`nav-item-${item}`} className="navigation-list-item">
          {active === index ? <div className="navigation-list-item-active" /> : null}
          <img className={`navigation-list-item-img-${index}`} src={navigationImg[index]} alt="" />
          <span className="navigation-list-item-text">{item}</span>
        </li>
      );
    });
  };

  render() {
    return (
      <nav>
        <div className="navigation-logo-wrapper">
          <div className="navigation-logo">
            <img src={logoPng} alt="logo" />
            <span className="navigation-logo-text">Open Circles</span>
          </div>
        </div>
        <ul className="navigation-list">{this.renderNavigationList()}</ul>
      </nav>
    );
  }
}

export default Navigate;
