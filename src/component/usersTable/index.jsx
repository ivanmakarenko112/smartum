import React, { Component } from 'react';

import CustomTableBlock from '@/blocks/customTableBlock';
import { columnName, users } from '@/mock/user';
class UsersTable extends Component {
  render() {
      const { handleClick } = this.props;
    return (
      <main>
        <CustomTableBlock handleClick={handleClick} rows={users} columns={columnName} />
        <button className="main-content-show-button">SHOW MORE</button>
      </main>
    );
  }
}

export default UsersTable;
