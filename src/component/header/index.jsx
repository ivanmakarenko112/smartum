import React, { Component } from 'react';

import burgerMenu from '@/assets/header/menu.png';
import searchImg from '@/assets/header/search.png';
import dropDawnMenu from '@/assets/header/drop-dawn.png';

class Header extends Component {
  render() {
    return (
      <header>
        <img className="menu" src={burgerMenu} alt="" />

        <div className="header-info">
          <span className="header-info-first">Users List</span>
          <span className="header-info-second">874 users</span>
        </div>

        <div className="search-wrapper">
          <img className="search-img" src={searchImg} alt="" />
          <input className="search-input" type="text" placeholder="Search" />
        </div>

        <button className="filter-button">Table filter</button>

        <div className="header-drop-down">
          <span className="header-drop-down-text">Dale McCormick</span>
          <img className="header-drop-down-img" src={dropDawnMenu} alt="" />
        </div>
      </header>
    );
  }
}

export default Header;
