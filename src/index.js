import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import Home from './page/Home';

ReactDOM.render(<Home />, document.getElementById('root'));
