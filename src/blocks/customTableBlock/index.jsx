import React, { Component } from 'react';

class CustomTableBlock extends Component {
  columnRender = () => {
    const { columns } = this.props;
    return columns.map((item, index) => {
      return (
        <th className={`columns-name-${item}`} key={`columns-${item}-${index}`}>
          {item}
        </th>
      );
    });
  };

  rowRender = () => {
    const { rows, columns, handleClick } = this.props;
    return rows.map((item, index) => {
      return (
        <tr onDoubleClick={handleClick.bind(null, item)} key={`row-${index}-${Math.random()}`}>
          {Object.values(item).map((value, index) => {
            const columnsName = columns[index];
            return columnsName === 'Name' ? (
              <td
                className={`table-block-column-${columnsName} columns-name-${columnsName}`}
                key={`${columnsName}-${value}-${Math.random()}`}
              >
                <span>{value.firstName}</span>
                <span className="table-block-second-name">{value.secondName}</span>
              </td>
            ) : (
              <td
                className={`table-block-column-${columnsName} columns-name-${columnsName}`}
                key={`${columnsName}-${value}-${Math.random()}`}
              >
                {value}
              </td>
            );
          })}
        </tr>
      );
    });
  };

  render() {
    return (
      <table className="table-block">
        <thead align="left" className="table-block-header">
          <tr>{this.columnRender()}</tr>
        </thead>
        <tbody className="table-block-body">{this.rowRender()}</tbody>
      </table>
    );
  }
}

export default CustomTableBlock;
