import React, { Component } from 'react';

class ModalDialog extends Component {
  render() {
    const { close } = this.props;
    return (
      <div onClick={close} className="dialog-wrapper">
        {this.props.children}
      </div>
    );
  }
}

export default ModalDialog;
